package com.example.handlerloopermessagequeue

import android.os.HandlerThread
import android.os.Process

class ExampleHandlerThread :
    HandlerThread("ExampleHandlerThread", Process.THREAD_PRIORITY_BACKGROUND) {

    lateinit var handler: ExampleHandler

    override fun onLooperPrepared() {
        handler = ExampleHandler(looper)
    }


}