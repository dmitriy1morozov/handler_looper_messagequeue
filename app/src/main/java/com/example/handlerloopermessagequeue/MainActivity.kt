package com.example.handlerloopermessagequeue

import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import com.example.handlerloopermessagequeue.ExampleHandler.Companion.TASK_A
import com.example.handlerloopermessagequeue.ExampleHandler.Companion.TASK_B

class MainActivity : AppCompatActivity() {

    private val looperThread = LooperThread()
    private val handlerThread = ExampleHandlerThread()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.btnStart).setOnClickListener(::start)
        findViewById<Button>(R.id.btnStop).setOnClickListener(::stop)
        findViewById<Button>(R.id.btnTaskA).setOnClickListener(::taskA)
        findViewById<Button>(R.id.btnTaskB).setOnClickListener(::taskB)

        findViewById<Button>(R.id.btnDoWork).setOnClickListener(::doWork)
        findViewById<Button>(R.id.btnRemoveMessages).setOnClickListener(::removeMessages)

        handlerThread.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        handlerThread.quitSafely()
    }

    private fun start(view: View?) {
        looperThread.start()
    }

    private fun stop(view: View?) {
        // Terminates processing message queue after processing the current message
        looperThread.looper.quit()
        // Finishes all the posted tasks from the message queue and then stops
//        looperThread.looper.quitSafely()
    }

    private fun taskA(view: View?) {
        Log.d(TAG, "taskA() called with: view = $view on a thread: ${Thread.currentThread()}")
        looperThread.handler.post(myRunnable)

        // This will post a single message to the looper's Handler
        val msg: Message = Message.obtain()
        msg.what = ExampleHandler.TASK_A
        looperThread.handler.sendMessage(msg)
    }

    private fun taskB(view: View?) {
        val msg: Message = Message.obtain()
        msg.what = ExampleHandler.TASK_B
        looperThread.handler.sendMessage(msg)
    }


    private fun doWork(view: View?) {
        handlerThread.handler.post(myRunnable)
        handlerThread.handler.sendEmptyMessage(TASK_A)
        handlerThread.handler.sendEmptyMessage(TASK_B)
    }

    private fun removeMessages(view: View?) {
        handlerThread.handler.removeMessages(TASK_A)

        // Passing null removes ALL runnables and messages
//        handlerThread.handler.removeCallbacksAndMessages(null)
    }


    companion object {
        val TAG = MainActivity::class.java.simpleName

        // static reference is used in order not to leak MainActivity via implicit reference
        // In case if we used anonymous inner class instead.
        val myRunnable = Runnable {
            for (i in 0..4) {
                Log.d(TAG, "Thread ${Thread.currentThread()} taskA: $i")
                SystemClock.sleep(1000)
            }
        }
    }
}