package com.example.handlerloopermessagequeue

import android.os.Handler
import android.os.Looper
import android.util.Log

class LooperThread : Thread() {

    lateinit var looper: Looper
    lateinit var handler: Handler

    override fun run() {
        Looper.prepare()
        looper = Looper.myLooper()!!
//        handler = Handler(looper)
        handler = ExampleHandler(looper)
        Looper.loop()

        Log.d(TAG, "run: END run()")
    }

    companion object {
        val TAG = LooperThread::class.java.simpleName
    }
}