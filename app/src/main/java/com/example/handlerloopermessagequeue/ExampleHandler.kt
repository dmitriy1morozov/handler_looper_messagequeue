package com.example.handlerloopermessagequeue

import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log

class ExampleHandler(looper: Looper) : Handler(looper) {

    override fun handleMessage(msg: Message) {
        when (msg.what) {
            TASK_A -> Log.d(TAG, "handleMessage: task A executed on a thread ${Thread.currentThread()}")
            TASK_B -> Log.d(TAG, "handleMessage: task B executed on a thread ${Thread.currentThread()}")
            else -> Log.d(TAG, "handleMessage: task UNKNOWN on a thread ${Thread.currentThread()}")
        }
    }

    companion object {
        val TAG = ExampleHandler::class.java.simpleName

        val TASK_A = 1
        val TASK_B = 2
    }
}