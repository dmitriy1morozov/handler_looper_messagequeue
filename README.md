https://youtube.com/playlist?list=PLrnPJCHvNZuDl4WUKi26WbzLMsM1sXCD4

## Handler, Thread, Looper, and MessageQueue
- Main thread is responsible for handling events from all over the app like callbacks associated with the lifecycle
- **Handlers**: For communicating between the threads. It enqueues task in the MessageQueue using Looper and also executes them when the task comes out of the MessageQueue.
- **Looper**: It is a worker that keeps a thread alive, loops through MessageQueue and sends messages to the corresponding handler to process.
- **Message Queue**: It is a queue that has tasks called messages which should be processed using these handler and Main thread looper works fine, but not good solution as it allocates lot of memory:
    a) We need to create a new thread every time we send data to the background 
    b) Every time we need to post data back to the main thread using the handler